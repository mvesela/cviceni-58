// plugin specification: https://prosemirror.net/docs/ref/#state.Plugin_System
export default class WrittingMenu {
  constructor(editorView, $menuTemplate) {
    this.$removeAllChanges = $menuTemplate.querySelector('.editor-toolbar-action-reset');

    this.init(editorView);
  }

  init(editorView) {
    const firstState = editorView.state;

    this.$removeAllChanges.addEventListener('click', () => {
      // eslint-disable-next-line no-alert, no-restricted-globals
      const isConfirmed = confirm('Opravdu chcete vrátit text do původní podoby? Vaše změny se tímto ztratí.');

      if (isConfirmed) {
        editorView.updateState(firstState);
      }
    });
  }

  static update(view, lastState) {
    // const { state, readOnly } = view;
  }

  destroy() {
    if (this.$removeAllChanges) {
      this.$removeAllChanges.remove();
    }
  }
}
